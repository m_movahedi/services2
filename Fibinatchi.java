package recursive;

public class Fibinatchi {

    public static void main(String[] args) {
        Fibinatchi fibinatchi = new Fibinatchi();
        for (int i = 0; i<10; i++)
        System.out.println(fibinatchi.nextFibo(i));
    }

    private int nextFibo(int n) {
        if(n < 0) {
            System.out.println("Please insert a positive number.");
        }
        if (n == 0 || n == 1) return 1;
        else
            return nextFibo(n - 1) + nextFibo(n - 2);
    }

}
